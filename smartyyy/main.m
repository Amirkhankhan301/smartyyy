//
//  main.m
//  smartyyy
//
//  Created by Amir on 03/12/2018.
//  Copyright © 2018 Amir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
